<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'admin'], function () {

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('users', App\Http\Controllers\UserController::class);
Route::resource('settings', App\Http\Controllers\SettingsController::class);
Route::resource('categories', App\Http\Controllers\CategoriesController::class);
Route::resource('testimonials', App\Http\Controllers\TestimonialsController::class);
Route::delete('/categories/{id}', [App\Http\Controllers\CategoriesController::class, 'destroy'])->name('categories.destroy');
Route::delete('/testimonials/{id}', [App\Http\Controllers\CategoriesController::class, 'destroy'])->name('testimonials.destroy');
});

Route::group(['middleware' => ['web']], function () {
    Route::get('/', [App\Http\Controllers\FrontendController::class, 'index'])->name('public.index');
    Route::get('/about', [App\Http\Controllers\FrontendController::class, 'about'])->name('public.about');
    Route::get('/portfolio', [App\Http\Controllers\FrontendController::class, 'portfolio'])->name('public.portfolio');
    Route::get('/contact', [App\Http\Controllers\FrontendController::class, 'contact'])->name('public.contact');
    Route::get('/services', [App\Http\Controllers\FrontendController::class, 'services'])->name('public.services');
});



