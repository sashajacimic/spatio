<?php

namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
            'logo_white' => '',
            'logo_black' => '',
            'meta_img' =>'',
            'url' => 'http://spatio.test',
            'title' => 'SPATIO',
            'meta_title' => 'SPATIO',
            'facebook' => 'www.fb.com/spatio',
            'instagram' => 'www.ig.com/spatio',
            'about_us' => 'Something about our company',
            'address' => 'Partizanski Odredi br.3/4',
            'email' => 'spatio@gmail.com',
            'phone' => '071353057',
            'desc' => 'Some description',
            'meta_desc' => 'Some Meta Description',
            'keywords' => 'Bla bla bla bla'

        ]);
    }
}
