<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('address');
            $table->string('title');
            $table->string('meta_title');
            $table->string('email');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('phone');
            $table->string('logo_white');
            $table->string('logo_black');
            $table->string('about_us');
            $table->string('meta_img');
            $table->string('meta_desc');
            $table->string('keywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
