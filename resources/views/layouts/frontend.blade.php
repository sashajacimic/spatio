<html lang="en">
<head>
    <!-- Title -->
    <title>Spatio</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="../../favicon.ico">
    <!-- Google Fonts -->
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/frontassets/vendor/bootstrap/bootstrap.min.css">
    <!-- CSS Global Icons -->
    <link rel="stylesheet" href="/frontassets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/frontassets/vendor/icon-line/css/simple-line-icons.css">
    <link rel="stylesheet" href="/frontassets/vendor/icon-etlinefont/style.css">
    <link rel="stylesheet" href="/frontassets/vendor/icon-line-pro/style.css">
    <link rel="stylesheet" href="/frontassets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/frontassets/vendor/animate.css">
    <link rel="stylesheet" href="/frontassets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/frontassets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/frontassets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/frontassets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/frontassets/vendor/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/frontassets/vendor/hs-bg-video/hs-bg-video.css">
    <link rel="stylesheet" href="/frontassets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="/frontassets/vendor/hamburgers/hamburgers.min.css">

    <!-- CSS Unify -->
    <link rel="stylesheet" href="/frontassets/css/unify-core.css">
    <link rel="stylesheet" href="/frontassets/css/unify-components.css">
    <link rel="stylesheet" href="/frontassets/css/unify-globals.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/frontassets/css/custom.css">
<body>
<main>

    <!-- Header -->
    <header id="js-header" class="u-header u-header--static">
        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
            <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
                <div class="container">
                    <!-- Responsive Toggle Button -->
                    <button
                        class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-minus-3 g-right-0"
                        type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar"
                        data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
                    </button>
                    <!-- End Responsive Toggle Button -->

                    <!-- Logo -->
                    <a href="/" class="navbar-brand d-flex" id="logo_white">
                        <img src="/assets/img/settings/thumbnails/{{$settings->logo_black}}" height="50px">
                    </a>
                    <!-- End Logo -->

                    <!-- Navigation -->
                    <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg"
                         id="navBar">
                        <ul class="navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto">
                            <!-- Home -->
                            <li class="hs-has-mega-menu nav-item  g-mx-10--lg g-mx-15--xl">
                                <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" aria-haspopup="true"
                                   aria-expanded="false" aria-controls="nav-submenu--features" href="/">Home</a>
                            </li>
                            <!-- End Home -->

                            <!-- Pages -->
                            <li class=" nav-item  g-mx-10--lg g-mx-15--xl">
                                <a id="nav-link-pages" class="nav-link  g-py-7 g-px-0" href="/about"
                                   aria-controls="nav-submenu-pages" aria-haspopup="true"
                                   aria-expanded="false">About us</a>
                            </li>
                            <!-- End Pages -->
                            <!-- Blog -->
                            <li class="nav-item   g-mx-10--lg g-mx-15--xl">
                                <a id="nav-link--blog" class="nav-link g-py-7 g-px-0" href="/portfolio">Portfolio</a>
                            </li>
                            <!-- End Blog -->

                            <!-- Features -->
                            <li class="nav-item hs-has-sub-menu  g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn"
                                data-animation-out="fadeOut">
                                <a id="nav-link--features" class="nav-link g-py-7 g-px-0" href="/services" aria-haspopup="true"
                                   aria-expanded="false" aria-controls="nav-submenu--features">Services</a>

                                <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-18 g-mt-8--lg--scrolling"
                                    id="nav-submenu--features" aria-labelledby="nav-link--features"
                                    style="display: none;">
                                    <!-- Features - Headers -->
                                    <li class="dropdown-item ">
                                        <a class="nav-link" href="../../unify-main/shortcodes/headers/index.html">Headers</a>
                                    </li>
                                    <!-- End Features - Headers -->

                                    <!-- Features - Promo blocks -->
                                    <li class="dropdown-item ">
                                        <a class="nav-link" href="../../unify-main/shortcodes/promo/index.html">Promo
                                            Blocks</a>
                                    </li>
                                    <!-- End Features - Promo blocks -->

                                    <!-- Features - Sliders -->
                                    <li class="dropdown-item hs-has-sub-menu ">
                                        <a id="nav-link--features--sliders" class="nav-link" href="#"
                                           aria-haspopup="true" aria-expanded="false"
                                           aria-controls="nav-submenu--features--sliders">Sliders</a>

                                        <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                                            id="nav-submenu--features--sliders"
                                            aria-labelledby="nav-link--features--sliders" style="display: none;">
                                            <li class="dropdown-item ">
                                                <a class="nav-link" href="../../revolution-slider/index.html">Revolution
                                                    sliders</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link" href="../../master-slider/index.html">Master
                                                    sliders</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-item hs-has-sub-menu ">
                                        <a id="nav-link--features--maps" class="nav-link" href="#" aria-haspopup="true"
                                           aria-expanded="false" aria-controls="nav-submenu--features--maps">Maps</a>

                                        <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                                            id="nav-submenu--features--maps" aria-labelledby="nav-link--features--maps"
                                            style="display: none;">
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/shortcode-base-google-maps.html">Google
                                                    Maps</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/shortcode-base-maps-with-pins.html">Maps
                                                    With Pins</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/shortcode-base-vector-maps.html">Vector
                                                    Maps</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- End Features - Maps -->

                                    <!-- Features - Footers -->
                                    <li class="dropdown-item hs-has-sub-menu ">
                                        <a id="nav-link--features--footers" class="nav-link" href="#"
                                           aria-haspopup="true" aria-expanded="false"
                                           aria-controls="nav-submenu--features--footers">Footers</a>

                                        <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                                            id="nav-submenu--features--footers"
                                            aria-labelledby="nav-link--features--footers" style="display: none;">
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/footers/shortcode-blocks-footer-classic.html">Classic
                                                    Footers</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/footers/shortcode-blocks-footer-contact-forms.html">Contact
                                                    Forms</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/footers/shortcode-blocks-footer-maps.html">Footers
                                                    Maps</a>
                                            </li>
                                            <li class="dropdown-item ">
                                                <a class="nav-link"
                                                   href="../../unify-main/shortcodes/footers/shortcode-blocks-footer-modern.html">Modern
                                                    Footers</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- End Features - Footers -->
                                </ul>
                            </li>
                            <!-- End Features -->

                            <!-- Shortcodes -->
                            <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                                <a href="/contact" class="nav-link g-py-7 g-px-0">Contact</a>
                            </li>
                            <!-- End Shortcodes -->

                        </ul>
                    </div>
                    <!-- End Navigation -->

                </div>
            </nav>
        </div>
    </header>
    <!-- End Header -->
@yield('content')
<!-- End News -->

    <hr class="g-brd-gray-light-v4 my-0">

    <div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
        <div class="container">
            <div class="row">
                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">About Us</h2>
                    </div>

                    <p>About Unify dolor sit amet, consectetur adipiscing elit. Maecenas eget nisl id libero tincidunt
                        sodales.</p>
                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Latest Posts</h2>
                    </div>

                    <article>
                        <h3 class="h6 g-mb-2">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Incredible template</a>
                        </h3>
                        <div class="small g-color-white-opacity-0_6">May 8, 2017</div>
                    </article>

                    <hr class="g-brd-white-opacity-0_1 g-my-10">

                    <article>
                        <h3 class="h6 g-mb-2">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">New features</a>
                        </h3>
                        <div class="small g-color-white-opacity-0_6">June 23, 2017</div>
                    </article>

                    <hr class="g-brd-white-opacity-0_1 g-my-10">

                    <article>
                        <h3 class="h6 g-mb-2">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">New terms and
                                conditions</a>
                        </h3>
                        <div class="small g-color-white-opacity-0_6">September 15, 2017</div>
                    </article>
                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Useful Links</h2>
                    </div>

                    <nav class="text-uppercase1">
                        <ul class="list-unstyled g-mt-minus-10 mb-0">
                            <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                                <h4 class="h6 g-pr-20 mb-0">
                                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">About Us</a>
                                    <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                                </h4>
                            </li>
                            <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                                <h4 class="h6 g-pr-20 mb-0">
                                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Portfolio</a>
                                    <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                                </h4>
                            </li>
                            <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                                <h4 class="h6 g-pr-20 mb-0">
                                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Our Services</a>
                                    <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                                </h4>
                            </li>
                            <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                                <h4 class="h6 g-pr-20 mb-0">
                                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Latest Jobs</a>
                                    <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                                </h4>
                            </li>
                            <li class="g-pos-rel g-py-10">
                                <h4 class="h6 g-pr-20 mb-0">
                                    <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Contact Us</a>
                                    <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                                </h4>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Our Contacts</h2>
                    </div>

                    <address class="g-bg-no-repeat g-font-size-12 mb-0"
                             style="background-image: url(/frontassets/img/maps/map2.png);">
                        <!-- Location -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-map-marker"></i>
              </span>
                            </div>
                            <p class="mb-0">795 Folsom Ave, Suite 600, <br> San Francisco, CA 94107 795</p>
                        </div>
                        <!-- End Location -->

                        <!-- Phone -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-phone"></i>
              </span>
                            </div>
                            <p class="mb-0">(+123) 456 7890 <br> (+123) 456 7891</p>
                        </div>
                        <!-- End Phone -->

                        <!-- Email and Website -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-globe"></i>
              </span>
                            </div>
                            <p class="mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover"
                                   href="mailto:info@htmlstream.com">info@htmlstream.com</a>
                                <br>
                                <a class="g-color-white-opacity-0_8 g-color-white--hover"
                                   href="#">www.htmlstream.com</a>
                            </p>
                        </div>
                        <!-- End Email and Website -->
                    </address>
                </div>
                <!-- End Footer Content -->
            </div>
        </div>
    </div>
    <!-- End Footer -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
                    <div class="d-lg-flex">
                        <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2020 &copy; All Rights
                            Reserved.</small>
                        <ul class="u-list-inline">
                            <li class="list-inline-item">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Privacy Policy</a>
                            </li>
                            <li class="list-inline-item">
                                <span>|</span>
                            </li>
                            <li class="list-inline-item">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Terms of Use</a>
                            </li>
                            <li class="list-inline-item">
                                <span>|</span>
                            </li>
                            <li class="list-inline-item">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">License</a>
                            </li>
                            <li class="list-inline-item">
                                <span>|</span>
                            </li>
                            <li class="list-inline-item">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Support</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 align-self-center">
                    <ul class="list-inline text-center text-md-right mb-0">
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top"
                            title="Facebook">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-skype"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top"
                            title="Linkedin">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top"
                            title="Pinterest">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top"
                            title="Dribbble">
                            <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Copyright Footer -->
    <a class="js-go-to u-go-to-v1" href="#" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
</main>

<div class="u-outer-spaces-helper"></div>


<!-- JS Global Compulsory -->
<script src="/frontassets/vendor/jquery/jquery.min.js"></script>
<script src="/frontassets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/frontassets/vendor/popper.js/popper.min.js"></script>
<script src="/frontassets/vendor/bootstrap/bootstrap.min.js"></script>


<!-- JS Implementing Plugins -->
<script src="/frontassets/vendor/appear.js"></script>
<script src="/frontassets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/frontassets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="/frontassets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontassets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontassets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/frontassets/vendor/hs-bg-video/hs-bg-video.js"></script>
<script src="/frontassets/vendor/hs-bg-video/vendor/player.min.js"></script>
<script src="/frontassets/vendor/fancybox/jquery.fancybox.js"></script>

<!-- JS Unify -->
<script src="/frontassets/js/hs.core.js"></script>
<script src="/frontassets/js/components/hs.header.js"></script>
<script src="/frontassets/js/helpers/hs.hamburgers.js"></script>
<script src="/frontassets/js/components/hs.tabs.js"></script>
<script src="/frontassets/js/helpers/hs.height-calc.js"></script>
<script src="/frontassets/js/components/hs.onscroll-animation.js"></script>
<script src="/frontassets/js/helpers/hs.bg-video.js"></script>
<script src="/frontassets/js/components/hs.popup.js"></script>
<script src="/frontassets/js/components/hs.carousel.js"></script>
<script src="/frontassets/js/components/hs.go-to.js"></script>

<!-- JS Customization -->
<script src="/frontassets/js/custom.js"></script>

<!-- JS Plugins Init. -->
<script>


    $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of header's height equal offset
        $.HSCore.helpers.HSHeightCalc.init();

        // initialization of scroll animation
        $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

        // initialization of video on background
        $.HSCore.helpers.HSBgVideo.init('.js-bg-video');

        // initialization of popups with media
        $.HSCore.components.HSPopup.init('.js-fancybox-media', {
            helpers: {
                media: {},
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, .8)'
                    }
                }
            }
        });

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
    });

    $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });
    });

    $(window).on('resize', function () {
        setTimeout(function () {
            $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
    });
</script>


<!-- CSS -->
<link rel="stylesheet" href="/frontassets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="/frontassets/vendor/chosen/chosen.css">
<link rel="stylesheet" href="/frontassets/vendor/prism/themes/prism.css">
<link rel="stylesheet" href="/frontassets/vendor/custombox/custombox.min.css">
<link rel="stylesheet" href="/frontassets/style-switcher/vendor/spectrum/spectrum.css">
<link rel="stylesheet" href="/frontassets/style-switcher/vendor/spectrum/themes/sp-dark.css">
<link rel="stylesheet" href="/frontassets/style-switcher/style-switcher.css">
<!-- End CSS -->

<!-- Scripts -->
<script src="/frontassets/vendor/chosen/chosen.jquery.js"></script>
<script src="/frontassets/vendor/image-select/src/ImageSelect.jquery.js"></script>
<script src="/frontassets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/frontassets/vendor/custombox/custombox.min.js"></script>
<script src="/frontassets/vendor/clipboard/dist/clipboard.min.js"></script>

<!-- Prism -->
<script src="/frontassets/vendor/prism/prism.js"></script>
<script src="/frontassets/vendor/prism/components/prism-markup.min.js"></script>
<script src="/frontassets/vendor/prism/components/prism-css.min.js"></script>
<script src="/frontassets/vendor/prism/components/prism-clike.min.js"></script>
<script src="/frontassets/vendor/prism/components/prism-javascript.min.js"></script>
<script src="/frontassets/vendor/prism/plugins/toolbar/prism-toolbar.min.js"></script>
<script src="/frontassets/vendor/prism/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js"></script>
<!-- End Prism -->

<script src="/frontassets/js/components/hs.scrollbar.js"></script>
<script src="/frontassets/js/components/hs.select.js"></script>
<script src="/frontassets/js/components/hs.modal-window.js"></script>
<script src="/frontassets/js/components/hs.markup-copy.js"></script>

<script src="/frontassets/style-switcher/vendor/cookiejs/jquery.cookie.js"></script>
<script src="/frontassets/style-switcher/vendor/spectrum/spectrum.js"></script>
<script src="/frontassets/style-switcher/style-switcher.js"></script>
<!-- End Scripts -->
<!-- End Style Switcher -->


</body>
</html>
