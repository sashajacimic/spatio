@extends('layouts.dash')
@section('content')
        <div class="col-12">
            <div class="table-responsive g-mb-40">
                <table class="table u-table--v3 g-color-black">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Company Name</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($testimonials as $testimonial)
                        <tr>
                            <td>{{ $testimonial->name }}</td>
                            <td>{{ $testimonial->company_name }}</td>
                            <td>{{ $testimonial->description }}</td>
                            <td><a class="btn btn-warning" href="/testimonials/{{ $testimonial->id }}/edit"><i class="hs-admin-pencil-alt"></i></a>
                            </td>
                            <td>
                                <form action="{{ route('testimonials.destroy', $testimonial) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger"><i class="hs-admin-eraser"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>




@endsection
