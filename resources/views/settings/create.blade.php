@extends('layouts.dash')


@section('content')

    <div class="container">
        <div class="row">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

            <div class="col-12">
                <form action="{{ route('settings.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="logo_white" class="col-md-4 col-form-label text-md-right">Logo White</label>
                        <div class="col-md-6">
                            <input id="logo_white" type="file" class="btn btn-warning" name="logo_white">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="logo_black" class="col-md-4 col-form-label text-md-right">Logo Black</label>
                        <div class="col-md-6">
                            <input id="logo_black" type="file" class="btn btn-warning" name="logo_black">

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="meta_img" class="col-md-4 col-form-label text-md-right">Meta Image</label>
                        <div class="col-md-6">
                            <input id="meta_img" type="file" class="btn btn-warning" name="meta_img">

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url" class="col-md-4 col-form-label text-md-right">Url</label>
                        <div class="col-md-6">
                            <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ old('url') }}"  required>
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}" name="title" required >
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="meta_title" class="col-md-4 col-form-label text-md-right">Meta Title</label>
                        <div class="col-md-6">
                            <input id="meta_title" type="text" class="form-control @error('meta_title') is-invalid @enderror" value="{{ old('meta_title') }}" name="meta_title" required >
                            @error('meta_title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook</label>
                        <div class="col-md-6">
                            <input id="facebook" type="text" class="form-control @error('facebook') is-invalid @enderror" value="{{ old('facebook') }}" name="facebook" required>
                            @error('facebook')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="instagram" class="col-md-4 col-form-label text-md-right">Instagram</label>
                        <div class="col-md-6">
                            <input id="instagram" type="text" class="form-control @error('instagram') is-invalid @enderror" value="{{ old('instagram') }}" name="instagram" required>
                            @error('instagram')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>
                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}" name="phone" required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}" name="address" required >
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="desc" class="col-md-4 col-form-label text-md-right">Description</label>
                        <div class="col-md-6">
                            <textarea id="desc" type="text" class="form-control" name="desc" required></textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="meta_desc" class="col-md-4 col-form-label text-md-right">Meta Description</label>
                        <div class="col-md-6">
                            <textarea id="meta_desc" type="text" class="form-control" name="meta_desc" required></textarea>
                            @error('meta_desc')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="about_us" class="col-md-4 col-form-label text-md-right">About Us</label>
                        <div class="col-md-6">
                            <textarea id="about_us" type="text" class="form-control" name="about_us" required></textarea>
                            @error('about_us')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="keywords" class="col-md-4 col-form-label text-md-right">Keywords</label>
                        <div class="col-md-6">
                            <textarea id="keywords" type="text" class="form-control" name="keywords" required></textarea>
                            @error('keywords')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>



                    <div class="form-group row">

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        CKEDITOR.replace( 'desc' );
        CKEDITOR.replace( 'about_us' );
        CKEDITOR.replace( 'meta_desc' );
    </script>
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
// Google Maps

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 41.9991801, lng: 21.38801309999999},
                zoom: 15
            });

            var marker = new google.maps.Marker({
                position: {lat: 41.99599107489828, lng: 21.43222198486329},
                map: map,
                draggable: true
            });
            var input = document.getElementById('searchmap');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);
            google.maps.event.addListener(searchBox, 'places_changed', function () {
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }
                map.fitBounds(bounds);
                map.setZoom(8);
            });
            google.maps.event.addListener(marker, 'position_changed', function () {
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();
                $('#lat').val(lat);
                $('#lng').val(lng);
            });
            $("form").bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("#searchmap").attr('value');
                    //add more buttons here
                    return false;
                }
            });
        });
    </script>

@endsection

