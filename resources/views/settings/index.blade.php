@extends('layouts.dash')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                URL</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->url}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                TITLE</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->title}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                META TITLE</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->meta_title}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                EMAIL</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->email}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                ADDRESS</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->address}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                PHONE NUMBER</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->phone}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                KEYWORDS</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->keywords}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                LOGO BLACK</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <img src="/assets/img/settings/thumbnails/{{$settings->logo_black}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                DESCRIPTION</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->desc}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                META DESCRIPTION</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->meta_desc}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                ABOUT US</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->about_us}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                FACEBOOK</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->facebook}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                INSTAGRAM</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <p class="g-font-weight-300 g-color-gray-dark-v6 mb-0">{{$settings->instagram}}</p>
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                LOGO WHITE</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <img src="/assets/img/settings/thumbnails/{{$settings->logo_white}}">
                    </div>
                </div>
                <div class="card g-brd-gray-light-v7 g-rounded-3 g-mb-30">
                    <header
                        class="card-header g-bg-transparent g-brd-bottom-none g-px-15 g-px-30--sm g-pt-15 g-pt-25--sm pb-0">
                        <div class="media">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0">
                                META IMAGE</h3>
                        </div>
                    </header>
                    <div class="card-block g-pa-15 g-pa-30--sm">
                        <img src="/assets/img/settings/thumbnails/{{$settings->logo_white}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

