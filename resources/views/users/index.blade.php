@extends('layouts.dash')

@section('content')




    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">All Users</h2>


                <div class="table-responsive g-mb-40">
                    <table class="table u-table--v3 g-color-black">
        <thead>
        <tr>
            <th>Full Name</th>
            <th>Role</th>
            <th>Email</th>
            <th>Created At</th>
            <th></th>
            <th></th>

        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->role->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at }}</td>
                <td><a class="btn btn-warning" href="/users/{{ $user->id }}/edit"><i class="hs-admin-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('users.destroy', $user) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger"><i class="hs-admin-eraser"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
