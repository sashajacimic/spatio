@extends('layouts.frontend')
@section('content')

    <!-- Promo Block -->
    <section class="g-flex-centered g-bg-pos-center g-bg-img-hero g-py-150"
             style="background-image: url(/images/slider1.jpg); height: calc(-75px + 100vh);"
             data-calc-target="#js-header">
        <div class="container text-center g-z-index-1">
            <h3 class="g-color-black g-font-weight-700 g-font-size-40 g-font-size-55--md text-uppercase g-mb-40 fadeInUp u-in-viewport"
                data-animation="fadeInUp" data-animation-delay="800" data-animation-duration="1500"
                style="animation-duration: 1500ms;">
                We are creative company
            </h3>
            <div data-animation="fadeInUp" data-animation-delay="1100" data-animation-duration="1500"
                 class="fadeInUp u-in-viewport" style="animation-duration: 2000ms;">
                <a class="btn btn-lg u-btn-outline-black g-font-weight-600 g-font-size-13 text-uppercase g-rounded-50 mx-2 g-px-25 g-py-15 g-mb-20"
                   href="#">Discover More</a>
                <a class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-50 mx-2 g-px-25 g-py-15 g-mb-20"
                   href="#">Contact Us</a>
            </div>
        </div>
    </section>
    <!-- End Promo Block -->

    <!-- About -->
    <section class="g-pt-100 g-pb-100">
        <div class="container">
            <!-- Image, Text Block -->
            <div class="row d-flex align-items-lg-center flex-wrap g-mb-60 g-mb-0--lg">
                <div class="col-md-6 col-lg-8">
                    <img class="img-fluid rounded" src="/images/900.jpg" alt="Image Description">
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="g-mt-minus-30 g-mt-0--md g-ml-minus-100--lg">
                        <div class="g-mb-20">
                            <h2 class="g-color-black g-font-weight-600 g-font-size-25 g-font-size-35--lg g-line-height-1_2 mb-4">
                                Finding the<br>Perfect Product
                            </h2>
                            <p class="g-font-size-16">This is where we sit down, grab a cup of coffee and dial in the
                                details. Understanding the task at hand and ironing out the wrinkles is key.</p>
                        </div>
                        <a class="btn u-btn-primary g-brd-2 g-brd-white g-font-size-13 g-rounded-50 g-pl-20 g-pr-15 g-py-9"
                           href="#">
                            More products
                            <span
                                class="align-middle u-icon-v3 g-width-16 g-height-16 g-color-black g-bg-white g-font-size-11 rounded-circle ml-3">
                    <i class="fa fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End Image, Text Block -->
        </div>

        <div class="container">
            <!-- Image, Text Block -->
            <div class="row d-flex justify-content-between align-items-lg-center flex-wrap g-mt-minus-50--lg">
                <div class="col-md-6 order-md-2">
                    <div class="g-brd-around--md g-brd-10 g-brd-white rounded">
                        <img class="img-fluid w-100 rounded" src="/images/600.jpg" alt="Image Description">
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 ml-auto order-md-1">
                    <div class="g-mt-minus-30 g-mt-0--md g-ml-minus-100--lg">
                        <div class="g-mb-20">
                            <h2 class="g-color-black g-font-weight-600 g-font-size-25 g-font-size-35--lg g-line-height-1_2 mb-4">
                                More than<br>a Stunning Look
                            </h2>
                            <p class="g-font-size-16">This is where we sit down, grab a cup of coffee and dial in the
                                details. Understanding the task at hand and ironing out the wrinkles is key.</p>
                        </div>
                        <a class="btn u-btn-primary g-brd-2 g-brd-white g-font-size-13 g-rounded-50 g-pl-20 g-pr-15 g-py-9"
                           href="#">
                            Read more
                            <span
                                class="align-middle u-icon-v3 g-width-16 g-height-16 g-color-black g-bg-white g-font-size-11 rounded-circle ml-3">
                    <i class="fa fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End Image, Text Block -->
        </div>
    </section>
    <!-- End About -->

    <!-- Icon Blocks -->
    <section class="g-bg-secondary">
        <div class="container g-pt-100 g-pb-130">
            <!-- Icon Blocks -->
            <div class="row no-gutters">
                <div class="col-sm-6 col-lg-3">
                    <div class="g-pr-40 g-mt-20">
                        <div class="g-mb-30">
                            <h2 class="h2 g-color-black g-font-weight-600 g-line-height-1_2 mb-4">What can<br>we
                                provide?</h2>
                            <p class="g-font-weight-300 g-font-size-16">The time has come to bring those ideas and plans
                                to life. This is where we really begin to visualize your napkin sketches and make them
                                into beautiful pixels.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="js-carousel slick-initialized slick-slider slick-dotted" data-infinite="true"
                         data-slides-show="3" data-slides-scroll="3" data-center-mode="true" data-center-padding="1px"
                         data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-minus-30"
                         data-responsive="[{
                     &quot;breakpoint&quot;: 992,
                     &quot;settings&quot;: {
                       &quot;slidesToShow&quot;: 2
                     }
                   }, {
                     &quot;breakpoint&quot;: 576,
                     &quot;settings&quot;: {
                       &quot;slidesToShow&quot;: 1
                     }
                   }]">
                        <div class="slick-list draggable" style="padding: 0px 1px;">
                            <div class="slick-track"
                                 style="opacity: 1; width: 3324px; transform: translate3d(-831px, 0px, 0px);">
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">01</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-087 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Creative</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="-3" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">02</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-035 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Features</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">03</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-141 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Responsive</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned slick-active" data-slick-index="-1"
                                     aria-hidden="false" tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">04</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-finance-256 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Code</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="0">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-current slick-active slick-center"
                                     data-slick-index="0" aria-hidden="false" tabindex="0" role="tabpanel"
                                     id="slick-slide00" aria-describedby="slick-slide-control00"
                                     style="width: 277px; height: auto;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">01</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-087 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Creative</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="0">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-active" data-slick-index="1" aria-hidden="false"
                                     tabindex="0" role="tabpanel" id="slick-slide01"
                                     aria-describedby="slick-slide-control01" style="width: 277px; height: auto;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">02</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-035 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Features</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="0">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide" data-slick-index="2" aria-hidden="true" tabindex="0"
                                     role="tabpanel" id="slick-slide02" aria-describedby="slick-slide-control02"
                                     style="width: 277px; height: auto;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">03</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-141 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Responsive</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1"
                                     role="tabpanel" id="slick-slide03" aria-describedby="slick-slide-control03"
                                     style="width: 277px; height: auto;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">04</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-finance-256 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Code</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned slick-center" data-slick-index="4"
                                     aria-hidden="true" tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">01</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-087 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Creative</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="5" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">02</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-035 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Features</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="6" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">03</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-education-141 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Responsive</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                                <div class="js-slide slick-slide slick-cloned" data-slick-index="7" aria-hidden="true"
                                     tabindex="-1" style="width: 277px;">
                                    <!-- Icon Blocks -->
                                    <div
                                        class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                                        <div class="mb-4">
                                            <span
                                                class="d-block g-color-gray-dark-v4 g-font-weight-700 text-right mb-3">04</span>
                                            <span
                                                class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                        <i class="icon-finance-256 u-line-icon-pro"></i>
                      </span>
                                            <h3 class="h5 g-color-black g-font-weight-600 mb-3">Code</h3>
                                            <p>This is where we sit down, grab a cup of coffee and dial in the
                                                details.</p>
                                        </div>
                                        <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover"
                                           href="#" tabindex="-1">Read more</a>
                                    </div>
                                    <!-- End Icon Blocks -->
                                </div>
                            </div>
                        </div>


                        <ul class="js-pagination u-carousel-indicators-v1 g-absolute-centered--x g-bottom-minus-30"
                            role="tablist" style="">
                            <li class="slick-active slick-current" role="presentation"><span></span></li>
                            <li role="presentation"><span></span></li>
                            <li role="presentation"><span></span></li>
                            <li role="presentation"><span></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Icon Blocks -->
        </div>
    </section>
    <!-- End Icon Blocks -->

    <!-- Mockup Block -->
    <section class="container g-py-100">
        <div class="text-center g-mb-70">
            <h2 class="h1 g-color-black g-font-weight-600">Multifanctional Magic</h2>
            <p class="lead">This is where we begin to visualize your napkin sketches and make them into pixels.</p>
        </div>

        <div class="row">
            <div class="col-md-3 g-mb-30">
          <span class="u-icon-v3 g-color-primary g-bg-primary-opacity-0_1 rounded-circle g-mb-25">
              <i class="icon-education-089 u-line-icon-pro"></i>
            </span>
                <div
                    class="d-inline-block g-width-70 g-height-2 g-bg-primary g-pos-rel g-top-5 g-right-minus-25 g-mr-minus-20"></div>
                <h2 class="h5 g-color-black g-font-weight-600 mb-3">Design is Functional</h2>
                <p>Now that we've aligned the details, it's time to get things mapped out and organized.</p>
            </div>

            <div class="col-md-6 g-mb-30">
                <img class="img-fluid" src="/frontassets/img-temp/900x650/img1.png" alt="Image Description">
            </div>

            <div class="col-md-3 mt-auto g-mb-30">
                <div
                    class="d-inline-block g-width-70 g-height-2 g-bg-brown g-pos-rel g-top-5 g-left-minus-25 g-mr-minus-20"></div>
                <span class="u-icon-v3 g-color-brown g-bg-brown-opacity-0_1 rounded-circle g-mb-25">
              <i class="icon-education-088 u-line-icon-pro"></i>
            </span>
                <h2 class="h5 g-color-black g-font-weight-600 mb-3">More than 200+ features</h2>
                <p>We aim high at being focused on building relationships with our clients and community.</p>
            </div>
        </div>
    </section>
    <!-- End Mockup Block -->

    <!-- Video Block -->
    <section class="container-fluid g-pa-20">
        <div
            class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-bg-cover g-bg-black-opacity-0_6--after"
            data-options="{direction: &quot;fromtop&quot;, animation_duration: 25, direction: &quot;reverse&quot;}">
            <!-- Promo Block - Parallax Video -->
            <div class="dzsparallaxer--target"
                 style="width: 1593px; height: 200%; transform: translate3d(0px, -450px, 0px);"
                 data-forcewidth_ratio="1.77">
                <div class="js-bg-video g-pos-abs w-100 h-100" data-hs-bgv-type="vimeo" data-hs-bgv-id="167434033"
                     data-hs-bgv-loop="1" style="position: relative;">
                    <div id="hsVimeoPlayer0" class="hs-vimeo hs-bg-video" data-hs-bgv-id="167434033"
                         data-hs-bgv-loop="1" data-vimeo-initialized="true">
                        <iframe
                            src="https://player.vimeo.com/video/167434033?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;autopause=0&amp;loop=1&amp;app_id=122963"
                            width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture"
                            allowfullscreen="" title="DESIGN DISRUPTORS Trailer #2 - A documentary from InVision"
                            data-ready="true" style="width: 2832px; height: 2080px;"></iframe>
                    </div>
                </div>
            </div>
            <!-- End Promo Block - Parallax Video -->

            <div class="row text-center g-min-height-450 g-flex-centered g-pos-rel g-z-index-1">
                <div class="col-sm-8 col-md-7 col-lg-4 px-5 px-sm-0">
                    <span class="d-block g-color-white-opacity-0_8 g-font-weight-600 text-uppercase mb-2">Intro</span>
                    <h2 class="h1 g-color-white g-font-weight-600 mb-0">This Week's Featured Video</h2>
                </div>
            </div>
        </div>
    </section>
    <!-- End Video Block -->

    <!-- Projects -->
    <section class="container g-py-100">
        <div class="text-center g-mb-50">
            <h2 class="h1 g-color-black g-font-weight-600">Branding Projects</h2>
            <p class="lead">This is where we begin to visualize your napkin sketches and make them into pixels.</p>
        </div>

        <div class="row no-gutters g-mx-minus-10">
            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-purple--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img1.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Clocks</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Alarm Clock</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-cyan--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img2.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Others</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Brochure</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-teal--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img7.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Hi-Tech</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Desk Clock</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-primary--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img10.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Cosmetics</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Cream</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-brown--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img5.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Hi-Tech</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Tivoli Radio</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-20">
                <!-- Projects -->
                <div
                    class="u-block-hover g-brd-around g-brd-gray-light-v4 g-color-black g-color-white--hover g-bg-pink--hover text-center rounded g-transition-0_3 g-px-30 g-py-50">
                    <img class="img-fluid u-block-hover__main--zoom-v1 mb-5"
                         src="/frontassets/img-temp/425x250/img6.png" alt="Image Description">
                    <span class="g-font-weight-600 g-font-size-12 text-uppercase">Cosmetics</span>
                    <h3 class="h4 g-font-weight-600 mb-0">Spa Cosmetics</h3>

                    <a class="u-link-v2" href="#"></a>
                </div>
                <!-- End Projects -->
            </div>
        </div>
    </section>
    <!-- End Projects -->

    <hr class="g-brd-gray-light-v4 my-0">

    <!-- News -->
    <section class="container g-pt-100 g-pb-90">
        <div class="text-center g-mb-50">
            <h2 class="h1 g-color-black g-font-weight-600">Latest News</h2>
            <p class="lead">This is where we begin to visualize your napkin sketches and make them into pixels.</p>
        </div>

        <div class="row g-mx-minus-10">
            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-10">
                <!-- Blog Background Overlay Blocks -->
                <article class="u-block-hover">
                    <div class="g-bg-cover g-bg-bluegray-gradient-opacity-v1--after">
                        <img class="d-flex align-items-end u-block-hover__main--mover-down"
                             src="/frontassets/img-temp/500x600/img1.jpg" alt="Image Description">
                    </div>
                    <div class="u-block-hover__additional--partially-slide-up text-center g-z-index-1 mt-auto">
                        <div class="u-block-hover__visible g-pa-25">
                            <span
                                class="d-block g-color-white-opacity-0_7 g-font-weight-600 g-font-size-12 mb-2">art</span>
                            <h2 class="h4 g-color-white g-font-weight-600 mb-3">
                                <a class="u-link-v5 g-brd-bottom g-brd-2 g-brd-white--hover g-color-white g-cursor-pointer g-pb-2"
                                   href="#">The Next Art Exhibition</a>
                            </h2>
                            <h4 class="d-inline-block g-color-white-opacity-0_7 g-font-size-11 mb-0">
                                By,
                                <a class="g-color-white-opacity-0_7 text-uppercase" href="#">Dan Shaw</a>
                            </h4>
                            <span class="g-color-white-opacity-0_7 g-pos-rel g-top-2 mx-2">·</span>
                            <span class="g-color-white-opacity-0_7 g-font-size-10 text-uppercase">May 31, 2017</span>
                        </div>

                        <a class="d-inline-block g-brd-bottom g-brd-white g-color-white g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover g-mb-30"
                           href="#">Read more</a>
                    </div>
                </article>
                <!-- End Blog Background Overlay Blocks -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-10">
                <!-- Blog Background Overlay Blocks -->
                <article class="u-block-hover">
                    <div class="g-bg-cover g-bg-bluegray-gradient-opacity-v1--after">
                        <img class="d-flex align-items-end u-block-hover__main--mover-down"
                             src="/frontassets/img-temp/500x600/img3.jpg" alt="Image Description">
                    </div>
                    <div class="u-block-hover__additional--partially-slide-up text-center g-z-index-1 mt-auto">
                        <div class="u-block-hover__visible g-pa-25">
                            <span
                                class="d-block g-color-white-opacity-0_7 g-font-weight-600 g-font-size-12 mb-2">travel</span>
                            <h2 class="h4 g-color-white g-font-weight-600 mb-3">
                                <a class="u-link-v5 g-brd-bottom g-brd-2 g-brd-white--hover g-color-white g-cursor-pointer g-pb-2"
                                   href="#">Time to Travel</a>
                            </h2>
                            <h4 class="d-inline-block g-color-white-opacity-0_7 g-font-size-11 mb-0">
                                By,
                                <a class="g-color-white-opacity-0_7 text-uppercase" href="#">Dan Shaw</a>
                            </h4>
                            <span class="g-color-white-opacity-0_7 g-pos-rel g-top-2 mx-2">·</span>
                            <span class="g-color-white-opacity-0_7 g-font-size-10 text-uppercase">May 31, 2017</span>
                        </div>

                        <a class="d-inline-block g-brd-bottom g-brd-white g-color-white g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover g-mb-30"
                           href="#">Read more</a>
                    </div>
                </article>
                <!-- End Blog Background Overlay Blocks -->
            </div>

            <div class="col-sm-6 col-lg-4 g-px-10 g-mb-10">
                <!-- Blog Background Overlay Blocks -->
                <article class="u-block-hover">
                    <div class="g-bg-cover g-bg-bluegray-gradient-opacity-v1--after">
                        <img class="d-flex align-items-end u-block-hover__main--mover-down"
                             src="/frontassets/img-temp/500x600/img2.jpg" alt="Image Description">
                    </div>
                    <div class="u-block-hover__additional--partially-slide-up text-center g-z-index-1 mt-auto">
                        <div class="u-block-hover__visible g-pa-25">
                            <span class="d-block g-color-white-opacity-0_7 g-font-weight-600 g-font-size-12 mb-2">photography</span>
                            <h2 class="h4 g-color-white g-font-weight-600 mb-3">
                                <a class="u-link-v5 g-brd-bottom g-brd-2 g-brd-white--hover g-color-white g-cursor-pointer g-pb-2"
                                   href="#">A Chair for Million</a>
                            </h2>
                            <h4 class="d-inline-block g-color-white-opacity-0_7 g-font-size-11 mb-0">
                                By,
                                <a class="g-color-white-opacity-0_7 text-uppercase" href="#">Dan Shaw</a>
                            </h4>
                            <span class="g-color-white-opacity-0_7 g-pos-rel g-top-2 mx-2">·</span>
                            <span class="g-color-white-opacity-0_7 g-font-size-10 text-uppercase">May 31, 2017</span>
                        </div>

                        <a class="d-inline-block g-brd-bottom g-brd-white g-color-white g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover g-mb-30"
                           href="#">Read more</a>
                    </div>
                </article>
                <!-- End Blog Background Overlay Blocks -->
            </div>
        </div>
    </section>
    <section class="container g-pt-100 g-pb-30">
        <div class="row justify-content-between">
            <div class="col-lg-12 g-mb-60">
                <h2 class="h3">Contact us</h2>
                <p class="g-color-gray-dark-v3 g-font-size-16">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>

                <hr class="g-my-40">

                <!-- Contact Form -->
                <form>
                    <div class="g-mb-10">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your name: <span class="g-color-red">*</span>
                        </label>
                        <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 rounded-3 g-py-13 g-px-15" type="text">
                    </div>

                    <div class="g-mb-20">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your email: <span class="g-color-red">*</span>
                        </label>
                        <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 rounded-3 g-py-13 g-px-15" type="email">
                    </div>

                    <div class="g-mb-40">
                        <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your message: <span class="g-color-red">*</span>
                        </label>
                        <textarea class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 g-resize-none rounded-3 g-py-13 g-px-15" rows="7"></textarea>
                    </div>

                    <button class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-default rounded-2 text-uppercase g-py-20 g-px-30" type="submit" role="button">Send Enquiry</button>
                </form>
                <!-- End Contact Form -->
            </div>

        </div>
    </section>
@endsection


