@extends('layouts.frontend')

@section('content')
    <html lang="en"><head>
        <!-- Title -->
        <title>Contacts 3 | | Unify - Responsive Website Template</title>

        <!-- Required Meta Tags Always Come First -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Favicon -->
        <link rel="shortcut icon" href="../../favicon.ico">
        <!-- Google Fonts -->
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">
        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="frontassets//vendor/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="frontassets//vendor/icon-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="frontassets//vendor/icon-line-pro/style.css">
        <link rel="stylesheet" href="frontassets//vendor/icon-hs/style.css">
        <link rel="stylesheet" href="frontassets//vendor/animate.css">
        <link rel="stylesheet" href="frontassets//vendor/dzsparallaxer/dzsparallaxer.css">
        <link rel="stylesheet" href="frontassets//vendor/dzsparallaxer/dzsscroller/scroller.css">
        <link rel="stylesheet" href="frontassets//vendor/dzsparallaxer/advancedscroller/plugin.css">
        <link rel="stylesheet" href="frontassets//vendor/hs-megamenu/src/hs.megamenu.css">
        <link rel="stylesheet" href="frontassets//vendor/hamburgers/hamburgers.min.css">

        <!-- CSS Unify -->
        <link rel="stylesheet" href="frontassets//css/unify-core.css">
        <link rel="stylesheet" href="frontassets//css/unify-components.css">
        <link rel="stylesheet" href="frontassets//css/unify-globals.css">

        <!-- CSS Customization -->
        <link rel="stylesheet" href="frontassets//css/custom.css">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script></head>

    <body>
    <main>



        <!-- Header -->
        <header id="js-header" class="u-header u-header--static">
            <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
                <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
                    <div class="container">
                        <!-- Responsive Toggle Button -->
                        <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-minus-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
                        </button>
                        <!-- End Responsive Toggle Button -->

                    </div>
                </nav>
            </div>
        </header>
        <!-- End Header -->

        <!-- Promo Block -->
        <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options="{direction: &quot;fromtop&quot;, animation_duration: 25, direction: &quot;reverse&quot;}">
            <!-- Parallax Image -->
            <div class="divimage dzsparallaxer--target w-100 u-bg-overlay g-bg-black-opacity-0_4--after" style="height: 140%; background-image: url(&quot;file:///Users/heisenberg/Downloads/templates/Unify-v2.6.3/html/assets/img-temp/1920x800/img6.jpg&quot;); transform: translate3d(0px, -37.60865px, 0px);"></div>
            <!-- End Parallax Image -->

            <!-- Promo Block Content -->
            <div class="container g-color-white text-center g-py-150">
                <h1 class="h2 g-font-weight-300">It is good to meet you</h1>
                <h2 class="display-2 g-font-weight-600 text-uppercase g-letter-spacing-1 mb-0">Contact us</h2>
            </div>
            <!-- Promo Block Content -->
        </section>
        <!-- End Promo Block -->

        <section class="container g-pt-100 g-pb-40">
            <div class="row justify-content-between">
                <div class="col-lg-7 g-mb-60">
                    <h2 class="h3">Contact us</h2>
                    <p class="g-color-gray-dark-v3 g-font-size-16">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>

                    <hr class="g-my-40">

                    <!-- Contact Form -->
                    <form>
                        <div class="g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your name: <span class="g-color-red">*</span>
                            </label>
                            <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 rounded-3 g-py-13 g-px-15" type="text">
                        </div>

                        <div class="g-mb-20">
                            <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your email: <span class="g-color-red">*</span>
                            </label>
                            <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 rounded-3 g-py-13 g-px-15" type="email">
                        </div>

                        <div class="g-mb-40">
                            <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Your message: <span class="g-color-red">*</span>
                            </label>
                            <textarea class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-primary--focus g-brd-gray-light-v4 g-resize-none rounded-3 g-py-13 g-px-15" rows="7"></textarea>
                        </div>

                        <button class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-default rounded-3 text-uppercase g-py-15 g-px-30" type="submit" role="button">Send Enquiry</button>
                    </form>
                    <!-- End Contact Form -->
                </div>

                <div class="col-lg-4 g-mb-60">
                    <!-- Google Map -->
                    <div id="GMapCustomized-light" class="js-g-map embed-responsive embed-responsive-21by9 g-height-300" data-type="custom" data-lat="40.674" data-lng="-73.946" data-zoom="12" data-title="Agency" data-styles="[[&quot;&quot;, &quot;&quot;, [{&quot;saturation&quot;:-100},{&quot;lightness&quot;:51},{&quot;visibility&quot;:&quot;simplified&quot;}]], [&quot;&quot;, &quot;labels&quot;, [{&quot;visibility&quot;:&quot;on&quot;}]], [&quot;water&quot;, &quot;&quot;, [{&quot;color&quot;:&quot;#bac6cb&quot;}]] ]" data-pin="true" data-pin-icon="frontassets//img/icons/pin/green.png">
                    </div>
                    <!-- End Google Map -->

                    <hr class="g-my-40">

                    <!-- Contact Info -->
                    <h2 class="h3 mb-4">Contact info</h2>
                    <div class="media mb-2">
                        <i class="d-flex g-color-gray-dark-v5 mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i>
                        <div class="media-body">
                            <p class="g-color-gray-dark-v3 mb-2">Unit 25 Suite 3, 925 Prospect PI
                                <br>New York Avenue,
                                <br>United States</p>
                        </div>
                    </div>
                    <div class="media mb-2">
                        <i class="d-flex g-color-gray-dark-v5 mt-1 mr-3 icon-communication-062 u-line-icon-pro"></i>
                        <div class="media-body">
                            <p class="g-color-gray-dark-v3 mb-2">Email: <a class="g-color-gray-dark-v4" href="#">htmlstream@support.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="media mb-2">
                        <i class="d-flex g-color-gray-dark-v5 mt-1 mr-3 icon-communication-033 u-line-icon-pro"></i>
                        <div class="media-body">
                            <p class="g-color-gray-dark-v3">Call: <span class="g-color-gray-dark-v4">+32 333 444 555</span>
                            </p>
                        </div>
                    </div>
                    <!-- End Contact Info -->

                    <hr class="g-my-40">

                    <a class="g-color-main g-color-black--hover g-text-underline--none--hover" href="#">
            <span class="align-middle u-icon-v2 u-icon-size--sm g-color-white g-bg-primary rounded-circle mr-3">
                <i class="g-font-size-default icon-finance-206 u-line-icon-pro"></i>
              </span>
                        <span class="g-font-weight-600 g-font-size-18">Live chat</span>
                    </a>
                </div>
            </div>
        </section>

        <a class="js-go-to u-go-to-v1 animated" href="#" data-type="fixed" data-position="{
     &quot;bottom&quot;: 15,
     &quot;right&quot;: 15
   }" data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn" style="display: none; position: fixed; bottom: 15px; right: 15px; opacity: 0;">
            <i class="hs-icon hs-icon-arrow-top"></i>
        </a>
    </main>

    <div class="u-outer-spaces-helper"></div>


    <!-- JS Global Compulsory -->
    <script src="frontassets//vendor/jquery/jquery.min.js"></script>
    <script src="frontassets//vendor/jquery-migrate/jquery-migrate.min.js"></script>
    <script src="frontassets//vendor/popper.js/popper.min.js"></script>
    <script src="frontassets//vendor/bootstrap/bootstrap.min.js"></script>


    <!-- JS Implementing Plugins -->
    <script src="frontassets//vendor/hs-megamenu/src/hs.megamenu.js"></script>
    <script src="frontassets//vendor/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="frontassets//vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="frontassets//vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
    <script src="frontassets//vendor/gmaps/gmaps.min.js"></script>

    <!-- JS Unify -->
    <script src="frontassets//js/hs.core.js"></script>
    <script src="frontassets//js/components/hs.header.js"></script>
    <script src="frontassets//js/helpers/hs.hamburgers.js"></script>
    <script src="frontassets//js/components/hs.tabs.js"></script>
    <script src="frontassets//js/components/hs.go-to.js"></script>
    <script src="frontassets//js/components/gmap/hs.map.js"></script>

    <!-- JS Customization -->
    <script src="frontassets//js/custom.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        // initialization of google map
        function initMap() {
            $.HSCore.components.HSGMap.init('.js-g-map');
        }

        $(document).on('ready', function () {
            // initialization of tabs
            $.HSCore.components.HSTabs.init('[role="tablist"]');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');
        });

        $(window).on('load', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });
        });

        $(window).on('resize', function () {
            setTimeout(function () {
                $.HSCore.components.HSTabs.init('[role="tablist"]');
            }, 200);
        });
    </script>

    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAtt1z99GtrHZt_IcnK-wryNsQ30A112J0&amp;callback=initMap" async="" defer=""></script>




    <div id="copyModal" class="text-left modal-demo g-bg-white g-color-black g-pa-20" style="display: none;"></div>

    <!-- CSS -->
    <link rel="stylesheet" href="frontassets//vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="frontassets//vendor/chosen/chosen.css">
    <link rel="stylesheet" href="frontassets//vendor/prism/themes/prism.css">
    <link rel="stylesheet" href="frontassets//vendor/custombox/custombox.min.css">
    <link rel="stylesheet" href="frontassets//style-switcher/vendor/spectrum/spectrum.css">
    <link rel="stylesheet" href="frontassets//style-switcher/vendor/spectrum/themes/sp-dark.css">
    <link rel="stylesheet" href="frontassets//style-switcher/style-switcher.css">
    <!-- End CSS -->

    <!-- Scripts -->
    <script src="frontassets//vendor/chosen/chosen.jquery.js"></script>
    <script src="frontassets//vendor/image-select/src/ImageSelect.jquery.js"></script>
    <script src="frontassets//vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="frontassets//vendor/custombox/custombox.min.js"></script>
    <script src="frontassets//vendor/clipboard/dist/clipboard.min.js"></script>

    <!-- Prism -->
    <script src="frontassets//vendor/prism/prism.js"></script>
    <script src="frontassets//vendor/prism/components/prism-markup.min.js"></script>
    <script src="frontassets//vendor/prism/components/prism-css.min.js"></script>
    <script src="frontassets//vendor/prism/components/prism-clike.min.js"></script>
    <script src="frontassets//vendor/prism/components/prism-javascript.min.js"></script>
    <script src="frontassets//vendor/prism/plugins/toolbar/prism-toolbar.min.js"></script>
    <script src="frontassets//vendor/prism/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js"></script>
    <!-- End Prism -->

    <script src="frontassets//js/components/hs.scrollbar.js"></script>
    <script src="frontassets//js/components/hs.select.js"></script>
    <script src="frontassets//js/components/hs.modal-window.js"></script>
    <script src="frontassets//js/components/hs.markup-copy.js"></script>

    <script src="frontassets//style-switcher/vendor/cookiejs/jquery.cookie.js"></script>
    <script src="frontassets//style-switcher/vendor/spectrum/spectrum.js"></script>
    <script src="frontassets//style-switcher/style-switcher.js"></script>
    <!-- End Scripts -->
    <!-- End Style Switcher -->




    </body></html>
@endsection
