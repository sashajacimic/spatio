<?php

namespace App\Http\Controllers;

use App\Models\Testimonials;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestimonialsController extends Controller
{

    public function index()
    {
        $testimonials = Testimonials::all();
        $data = ['testimonials' => $testimonials];
        return view('testimonials.index')->with($data);

    }


    public function create()
    {
        return view('testimonials.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'company_name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {

            return redirect('testimonials/create')
                ->withErrors($validator)
                ->withInput();
        }
        Testimonials::create([

            'name' => $request->get('name'),
            'company_name' => $request->get('company_name'),
            'description' => $request->get('description')

        ]);

        $testimonials = Testimonials::all();
        $data = ['testimonials' => $testimonials];
        return view('testimonials.index')->with($data);
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $testimonial = Testimonials::FindOrFail($id);
        $data = ['testimonial' => $testimonial];
        return view('testimonials.edit')->with($data);
    }

    public function update(Request $request, $id)
    {

        $testimonial = Testimonials::FindOrFail($id);
        $testimonial->update([
            'name'         => $request->get('name'),
            'company_name' => $request->get('company_name'),
            'desc'         => $request->get('desc'),
        ]);

        $testimonials = Testimonials::all();
        $data = ['testimonials' => $testimonials];
        return view('testimonials.index')->with($data);
    }

    public function destroy($id)
    {
        $testimonials = Testimonials::FindOrFail($id);
        $data = ['testimonials' => $testimonials];
        return view('testimonials.index')->with($data);
    }
}
