<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Settings;
use App\Models\Testimonials;

class FrontendController extends Controller
{
    public function index()
    {
        $settings = Settings::first();
        $testimonials = Testimonials::all();
        $data = ['settings' => $settings, 'testimonials' => $testimonials];
        return view('frontend.index')->with($data);
    }

    public function about()
    {
        $settings = Settings::first();
        $data = ['settings' => $settings];
        return view('frontend.about')->with($data);
    }

    public function portfolio()
    {
        $settings = Settings::first();
        $categories = Categories::all();
        $data = ['categories' => $categories, 'settings' => $settings];
        return view('frontend.portfolio')->with($data);
    }

    public function contact()
    {
        $settings = Settings::first();
        $data = ['settings' => $settings];
        return view('frontend.contact')->with($data);
    }

    public function services()
    {
        $settings = Settings::first();
        $data = ['settings' => $settings];
        return view('frontend.services')->with($data);
    }
}
