<?php


namespace App\Http\Controllers\ViewComposer;

use App\Models\Settings;
use Illuminate\View\View;

class SettingsComposer
{
    public function compose(View $view)
    {
        $view->with('settings', Settings::first());
    }

}
