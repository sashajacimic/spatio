<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function index()
    {
        $users = User::paginate(5);
        $roles = Roles::all();
        $data = ["users" => $users, "roles" => $roles];
        return view('users.index')->with($data);

    }

    public function create()
    {
        $roles = Roles::all();
        $data = ["roles" => $roles];
        return view('users.create')->with($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'role_id' => ['required']]
        );

        if ($validator->fails()) {
            return redirect('user/create')
                ->withErrors($validator)
                ->withInput();
        }

        User::create([

            'name' => $request->get('name'),
            'email'      => $request->get('email'),
            'role_id'    => $request->get('role_id'),
            'password'   => Hash::make($request->get('password'))

        ]);

        Session::flash('flash_message', 'User successfully created!');
        $users = User::all();
        $data = ["users" => $users];
        return view('users.index')->with($data);

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::FindOrFail($id);
        $roles = Roles::all();
        $data = ["user" => $user, "roles" => $roles];
        return view('users.edit')->with($data);
    }


    public function update(Request $request, User $user)
    {
        if ($request->has('password') && !empty($request->get('password'))) {
            $input['password'] = Hash::make($request->get('password'));
        } else {
            $input = $request->except(['password']);
        }
        $user->fill($input)->save();
        $users = User::all();
        $data = ['users' => $users];
        return view('users.index')->with($data);
    }

    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        $user->delete();
        $users = User::all();
        $data = ["users" => $users];
        return view('users.index')->with($data);
    }
}
