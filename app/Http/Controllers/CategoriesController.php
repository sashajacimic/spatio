<?php

namespace App\Http\Controllers;

use App\Http\Helper\ImageStore;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Categories::getTree();
        $data = ['categories' => $categories];
        return view('categories.index')->with($data);
    }


    public function create()
    {
        $categories = Categories::all();
        $data = ['categories' => $categories];
        return view('categories.create')->with($data);
    }


    public function store(Request $request)
    {
        $name = $request->get('name');
        $parent_id = $request->get('parent_id');
        $image = $request->get('image');
        $slug = Str::slug($request->get('name'));


        $imageObj = new ImageStore($request, 'categories');
        $image = $imageObj->imageStore();


        if (!isset($parent_id)) {
            $data = [
                'name' => $name,
                'image' => $image,
                'slug' => $slug
            ];
            Categories::create($data);
            Session::flash('flash_message', 'Category successfully added!');
            return redirect()->back();
        }

        $category = Categories::FindOrFail($parent_id);

        Categories::create(['name' => $name, 'image' => $image], $category);
        Session::flash('flash_message', 'Category successfully added!');
        return redirect()->back();
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {


        $category = Categories::FindOrFail($id);
        $categories = Categories::getList();
        $data = ['category' => $category, 'categories' => $categories];
        return view('categories.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $category = Categories::FindOrFail($id);
        $input = $request->all();
        $category->fill($input)->save();
        Session::flash('flash_message', 'Category successfully edited.');

        $categories = Categories::getTree();
        $data = ['categories' => $categories];
        return view('categories.index')->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Categories::FindOrFail($id);
        $category->delete();
        $categories = Categories::all();
        $data = ['categories' => $categories];
        return redirect('admin/categories')->with($data);
    }
}
