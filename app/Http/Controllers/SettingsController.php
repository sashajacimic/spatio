<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Helper\ImageStore;

class SettingsController extends Controller
{

    public function index()
    {

        $settings = Settings::first();
        if ($settings) {
            return view('settings.index')->with(['settings' => $settings]);
        }
        return view('settings.create');
    }

    public function create()
    {
        return view('settings.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => ['required', 'string', 'max:255'],
            'logo_white' => ['required', 'image'],
            'logo_black' => ['required', 'image'],
            'meta_img' => ['required', 'image'],
            'title' => ['required', 'string', 'max:255'],
            'meta_title' => ['required', 'string', 'max:255'],
            'facebook' => ['required', 'string', 'max:255'],
            'instagram' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'meta_desc' => ['required', 'string', 'max:255'],
            'keywords' => ['required', 'string', 'max:255'],
            'about_us' => ['required', 'string', 'max:255']

        ]);
        if ($validator->fails()) {

            return redirect('/admin/settings/create')
                ->withErrors($validator)
                ->withInput();
        }


        $imageObj = new ImageStore($request, 'settings');
        $logo_white = $imageObj->imageStore('logo_white');

        $metaImgObj = new ImageStore($request, 'settings');
        $logo_black = $metaImgObj->imageStore('logo_black');


        $logoWhiteObj = new ImageStore($request, 'settings');
        $meta_img = $logoWhiteObj->imageStore('$meta_img');

            Settings::create([

                'url' => $request->get('url'),
                'logo_white' => $logo_white,
                'logo_black' => $logo_black,
                'meta_img' => $meta_img,
                'title' => $request->get('title'),
                'meta_title' => $request->get('meta_title'),
                'facebook' => $request->get('facebook'),
                'instagram' => $request->get('instagram'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'desc' => $request->get('desc'),
                'meta_desc' => $request->get('meta_desc'),
                'keywords' => $request->get('keywords'),
                'about_us' => $request->get('about_us')
            ]);

            Session::flash('flash_message', 'Settings successfully created!');
            return redirect()->back();
        }


    public function edit()
    {
        $settings = Settings::first();
        $data = ['settings' => $settings];
        return view('settings.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $settings = Settings::FindOrFail($id);
        $input = $request->all();

        if ($request->hasFile('logo_white')) {
            $imageObj = new ImageStore($request, 'settings');
            $logo_white = $imageObj->imageStore();
            $input['logo_white'] = $logo_white;
        }
        if ($request->hasFile('meta_img')) {

            $metaImgObj = new ImageStore($request, 'settings');
            $meta_img = $metaImgObj->imageStore('meta_img');
            $input['meta_img'] = $meta_img;
        }
        if ($request->hasFile('logo_black')) {

            $logoWhiteObj = new ImageStore($request, 'settings');
            $logo_black = $logoWhiteObj->imageStore('logo_black');
            $input['logo_black'] = $logo_black;
        }

        $settings->fill($input)->save();

        return view('settings.index')->with(['settings' => $settings]);
    }



    public function destroy($id)
    {
        $settings = Settings::FindOrFail($id);
        $settings->delete();
        return view('settings.create');
    }
}
