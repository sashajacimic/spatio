<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Settings extends Model
{
    use HasFactory;

    protected $fillable = [

        'url',
        'logo_white',
        'logo_black',
        'title',
        'meta_title',
        'facebook',
        'instagram',
        'email',
        'phone',
        'address',
        'desc',
        'meta_desc',
        'keywords',
        'about_us',
        'meta_img'
    ];
}


