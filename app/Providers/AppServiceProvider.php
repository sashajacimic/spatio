<?php

namespace App\Providers;

use App\Http\Controllers\ViewComposer\SettingsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer('layouts.dash', SettingsComposer::class);
    }
}
